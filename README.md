# Projeto Web

![preview](./assets/ProfileArthur.png)

> Trilha Explorer

Projeto construído Pelo Arthur covelo com muito 💛 da Rocketseat.

[🔗 Clique aqui para acessar](https://arthurcovelo.github.io/ProjetoWeb_Profile/)

## 🛠 Tecnologias

- [![HTML](https://img.shields.io/badge/HTML-5-orange?style=flat&logo=html5&logoColor=white)](https://www.w3.org/html/)
- [![CSS](https://img.shields.io/badge/CSS-3-blue?style=flat&logo=css3&logoColor=white)](https://www.w3.org/Style/CSS/)
- [![JavaScript](https://img.shields.io/badge/JavaScript-ES6-yellow?style=flat&logo=javascript&logoColor=white)](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [![Git](https://img.shields.io/badge/Git-000000?style=flat&logo=git&logoColor=white)](https://git-scm.com/)
- [![Figma](https://img.shields.io/badge/Figma-F24E1E?style=flat&logo=figma&logoColor=white)](https://www.figma.com/)

## 💛 Contato

Arthurcovelo@gmail.com
